package main

import (
	"fmt"
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

const (
	idx_html string = `
<html>
<head>
<script>
function copy() {
  var copyText = document.getElementById("clipboard");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  navigator.clipboard.writeText(copyText.value);
}
</script>
</head>

<body>
<form action="/clipboard" method="POST">

<textarea rows="25" cols="80" name="clipboard" id="clipboard">{{.Clipboard}}</textarea>
<input type="submit"></input>
</form>

<button onclick="copy()">Copy</button>

<br></br>
<hr>

<pre>
<code>
# replace "thisisatest" with data
curl -i -d 'clipboard=thisisatest' -X POST gauss.johnmarianhoffman.com:24761/clipboard

# Change single quotes to backticks (formatting/string literal issues on the go side of things 🙄)
# change go.mod to filename to upload
echo clipboard='cat go.mod' | curl -i -d @- -X POST localhost:8080/clipboard
</code>
</pre>

</body>

<html>`
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	t, _ := template.New("index").Parse(idx_html)
	err := t.Execute(w, struct{ Clipboard string }{Clipboard: clipboard})
	if err != nil {
		log.Fatal(err)
	}
}

func ClipboardHandler(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodGet:
		fmt.Fprintf(w, "%s", clipboard)
		log.Info("responded with clipboard: ", clipboard)
	case http.MethodPost:
		r.ParseMultipartForm(10_000_000)

		if !r.Form.Has("clipboard") {
			http.Error(w, "missing clipboard data", http.StatusInternalServerError)
			return
		}

		clipboard = r.Form.Get("clipboard")
		log.Info("set clipboard: ", clipboard)
		http.Redirect(w, r, "/", http.StatusFound)
	}

}

var clipboard string

func main() {

	r := mux.NewRouter()
	r.HandleFunc("/", IndexHandler)
	r.HandleFunc("/clipboard", ClipboardHandler)

	addr := ":8080"
	log.Info("Starting server on ", addr)
	log.Fatal(http.ListenAndServe(addr, r))
}
