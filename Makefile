run:
	go build	
	docker compose build
	docker compose up 

deploy:
	GOOS=linux GOARCH=amd64 go build
	docker compose build
	docker compose up -d

