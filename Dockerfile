FROM alpine:latest
RUN mkdir /app
WORKDIR /app
COPY clipboard-server /app/
ENTRYPOINT ["/app/clipboard-server"]
